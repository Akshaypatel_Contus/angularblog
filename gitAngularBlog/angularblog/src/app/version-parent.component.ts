import { Component } from '@angular/core';

@Component({
  selector: 'app-version-parent',
  template: `
    <h2>Source code version</h2>
    <button (click)="newMinor1()">New sub-minor version</button>
    <button (click)="newMinor()">New minor version</button>
    <button (click)="newMajor()">New major version</button>
    <app-version-child [major]="major" [minor]="minor" [minor1]="minor1"></app-version-child>
  `
})
export class VersionParentComponent {
  major = 1;
  minor = 1;
  minor1=1

  newMinor() {
    this.minor++;
    this.minor1=1
  }
  newMinor1()
  {
    this.minor1++
  }

  newMajor() {
    this.major++;
    this.minor = 0;
    this.minor1=1
  }
}


