import { Component, Input, OnInit } from '@angular/core';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Blog';
  user=localStorage.getItem("currentUser");
  
  @Input('appHighlight') highlightColor: string;
  value='';
  onEnter(value:string){
  this.value=value;
  }

}