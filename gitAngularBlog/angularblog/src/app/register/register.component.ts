import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {Routes, RouterModule, Router} from "@angular/router";
import { Observable } from 'rxjs/Observable';
import { NgModel } from '@angular/forms';
import 'rxjs/add/operator/map';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  model: any = {};
  constructor(private http: HttpClient,private route: Router) { }
  
  loading:false;
  ngOnInit() {

  }
  register()
  {
    console.log(this.model.fname);
    console.log(this.model.email);
    console.log(this.model.password);
  	this.http.post('http://127.0.0.1:8000/api/insertuser',{name: this.model.fname, email: this.model.email, password: this.model.password }).subscribe(user => {
        this.route.navigateByUrl('/login');
  	});
  }

}
