import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {Routes, RouterModule, Router,ActivatedRoute} from "@angular/router";
import { Observable } from 'rxjs/Observable';
import { NgModel } from '@angular/forms';
import 'rxjs/add/operator/map';


@Component({
  selector: 'app-blog-view',
  templateUrl: './blog-view.component.html',
  styleUrls: ['./blog-view.component.css']
})
export class BlogViewComponent implements OnInit {
	
	comment:any=[];
  replyComment:any=[];
  replycmt:any=[];
	hero:any=[];
	model:any={};
  user=localStorage.getItem("currentUser");
  //user1=localStorage.getItem("currentUser");
  constructor(private http: HttpClient,private route: Router,private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  	let id=this.activatedRoute.snapshot.paramMap.get('id');
    console.log(id); // Print the parameter to the console. 
    this.http.post('http://127.0.0.1:8000/api/editblog',{id:id}).subscribe(data => {
      //console.log(data);
      this.hero=data;
      //console.log(this.hero);
    });
    this.loadcomment();
  }
  loadcomment()
  {
    let id=this.activatedRoute.snapshot.paramMap.get('id');
    this.http.post('http://127.0.0.1:8000/api/loadcomment',{id:id}).subscribe(data => {
      
      this.comment=data;
      console.log(this.comment);
      /*for(let comment of this.comment)
      {
        this.http.post('http://127.0.0.1:8000/api/loadreplycomment',{id:comment.id}).subscribe(data1 => {
          this.replycmt=data1;
          console.log(this.replycmt);
          console.log(comment.id);
          console.log(this.replycmt[0].id);
          for(let reply of this.replycmt)
          {
            this.replyComment[comment.id][reply.id]=data1;
          }
        });
        console.log(this.replyComment);
      }*/
      
    });
    this.http.post('http://127.0.0.1:8000/api/loadreplycomment',{}).subscribe(data => {
      
      this.replyComment=data;
      console.log(this.replyComment);
    });
  }
  addcomment()
  {
  	let id=this.activatedRoute.snapshot.paramMap.get('id');
  	const formData: FormData = new FormData();
    formData.append('comment',this.model.comments);
    formData.append('blog_id',id);
    formData.append('email',localStorage.getItem('currentUser'));
    
    console.log(this.model.comments);
  	//console.log(formData);
  	this.http.post('http://127.0.0.1:8000/api/add_comment',formData).subscribe(data => {
      //console.log(data);
      this.comment=data;
      console.log(this.comment);
    });
  }
  deletecomment(id:string)
  {
    this.http.post('http://127.0.0.1:8000/api/delete_comment',{id:id}).subscribe(data => {
      //console.log(data);
      this.loadcomment();
    });
  }
  addreplycomment(id:string)
  {
    const formData: FormData = new FormData();
    formData.append('replycmt',this.model.recomments);
    formData.append('cmt_id',id);
    formData.append('email',localStorage.getItem('currentUser'));
    console.log(this.model.comments);
    //console.log(formData);
    this.http.post('http://127.0.0.1:8000/api/add_reply_comment',formData).subscribe(data => {
      this.loadcomment();
    });
  }
  delete_reply_comment(id:string)
  {
    this.http.post('http://127.0.0.1:8000/api/delete_reply_comment',{id:id}).subscribe(data => {
      //console.log(data);
      this.loadcomment();
    });
  }
}
