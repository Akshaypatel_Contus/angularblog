

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import {NgxPaginationModule} from 'ngx-pagination';
import { BrowserAnimationsModule,NoopAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';

import { BlogsComponent } from './blogs/blogs.component';

import { EditComponent } from './blogs/edit.component';


import { AppRoutingModule } from './/app-routing.module';
import { BlogViewComponent } from './blog-view/blog-view.component';

import { PagerService } from './service/index';

import { VersionChildComponent } from './version-child.component';
import { VersionParentComponent } from './version-parent.component';
import { AuthService } from './login/auth.service';
import { AuthGuard } from './auth.guard';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AddBlogComponent } from './add-blog/add-blog.component';
import { AppHeaderComponent } from './appheader.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { HeroListComponent } from './hero-list/hero-list.component';
import { HeroService }         from './hero.service';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserService } from './user.service';
import { UsersComponent } from './users/users.component';
import { UsersModule } from './users/users.module'




const appRoutes: Routes = [
		  {path: '', redirectTo: '/blogs', pathMatch: 'full' },
  		{ path: 'blogs', component: BlogsComponent,canActivate: [AuthGuard] },
      { path: 'addblogs', component: AddBlogComponent,canActivate: [AuthGuard] },
  		{ path: 'blogs/blogview/:id', component: BlogViewComponent },
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'blogs/edit/:id', component: EditComponent,canActivate: [AuthGuard] },
      { path: 'blogs/blogview/:ids/edit/:id', component: EditComponent,canActivate: [AuthGuard] },
      {
        path: 'users',
        loadChildren:'app/users/users.module#UsersModule',
      }
	];

@NgModule({
  declarations: [
    AppComponent,
    BlogsComponent,
    BlogViewComponent,
    VersionChildComponent,
    VersionParentComponent,
    LoginComponent,
    RegisterComponent,
    AddBlogComponent,
    AppHeaderComponent,
    EditComponent,
    HeroDetailComponent,
    HeroListComponent,
    UserDetailComponent,
    UserListComponent,
    UsersComponent,
    
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    UsersModule
  ],
  providers: [AuthService,AuthGuard,PagerService,HeroService,UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
