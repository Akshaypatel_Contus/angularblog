import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users.component';
import { UserdetailsComponent } from './userdetails/userdetails.component';
import { MyusersComponent } from './myusers/myusers.component';
import { AuthGuard } from '../auth.guard';
const routes: Routes = [
	{
		path:'',
		component:UsersComponent,
		children:[
			{
				path:'userdetail' , component:UserdetailsComponent
			},
			{
				path:'myusers',component:MyusersComponent,canActivate: [AuthGuard]
			},
			
		]
	}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
