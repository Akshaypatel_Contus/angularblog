import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UserdetailsComponent } from './userdetails/userdetails.component';
import { MyusersComponent } from './myusers/myusers.component';

@NgModule({
  imports: [
    CommonModule,
    UsersRoutingModule
  ],
  declarations: [UserdetailsComponent, MyusersComponent]
})
export class UsersModule { }
