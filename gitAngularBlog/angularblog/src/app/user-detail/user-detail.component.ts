import { Component, OnInit,Input,OnChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';

import { Address, Userdata, states } from '../userdata';
import { UserService }           from '../user.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
	@Input() users = Userdata;
	userForm:FormGroup;
	states=states;

  constructor(private fb:FormBuilder,private userservice:UserService) {
  	this.createForm();
   }
  ngOnInit() {
  }
  createForm()
  {
  	this.userForm=this.fb.group({
  		name:'',
  		email:'',
  		mobileno:'',
  		addresslist:this.fb.array([])
  	});
  }
  ngOnChanges()
  {
  	this.rebuildForm();
  }
  rebuildForm()
  {
  	console.log(this.users.email);
  	this.userForm.reset({
  		name:this.users.name,
  		email:this.users.email,
  		mobileno:this.users.mobileno
  	});
  	this.setAddress(this.users.address);
  }
  setAddress(addresses: Address[]) {
    const addressFGs = addresses.map(address => this.fb.group(address));
    const addressFormArray = this.fb.array(addressFGs);
    this.userForm.setControl('addform', addressFormArray);
  }
  get addform(): FormArray {
    return this.userForm.get('addform') as FormArray;
  };
  prepareSaveHero(): Userdata {
    const formModel = this.userForm.value;

    // deep copy of form model lairs
    const secretLairsDeepCopy: Address[] = formModel.addform.map(
      (address: Address) => Object.assign({}, address)
    );

    // return new `User` object containing a combination of original user value(s)
    // and deep copies of changed form model values
    const saveUser: Userdata = {
      id: this.users.id,
      name: formModel.name as string,
      email:formModel.email as string,
      mobileno:formModel.mobileno as string,
      // addresses: formModel.secretLairs //
      address: secretLairsDeepCopy
    };
    return saveUser;
  }
  onSubmit() {
    this.users = this.prepareSaveHero();
    this.userservice.updateUsers(this.users).subscribe();
    this.rebuildForm();
  }
  addAddress() {
    this.addform.push(this.fb.group(new Address()));
  }

}
