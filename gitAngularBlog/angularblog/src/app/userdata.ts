export class Userdata {
	id = 0;
  	name = '';
  	email='';
  	mobileno='';
  	address: Address[];
}
export class Address{
	street = '';
  	city   = '';
  	state  = '';
  	zip    = '';
  	country='';
}
export const users:Userdata[]=[
	{
		id: 1,
    	name: 'Akshay Patel',
    	email:'akky.akshu@outlook.com',
    	mobileno:'7990534970',
    	address: [
      		{street: 'Plot-2,Manohar nagar',  city: 'Chennai', state: 'Tamilnadu',  zip: '600100',country:'India'},
      		{street: 'Opp Marfatiyas Dela,Viramgam', city: 'Ahamedabad', state: 'Gujarat', zip: '382150',country:'India'},
    	]
	},
	{
   	 	id: 2,
    	name: 'Vivek Patel',
    	email:'vivek@outlook.com',
    	mobileno:'7990534970',
    	address: [
      		{street: 'Viramgam',  city: 'Ahmedabad', state: 'Gujarat',  zip: '382150',country:'India'}    	
      	]
  }
]
export const states = ['Gujarat', 'Tamilnadu'];
