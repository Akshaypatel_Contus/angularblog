import { Component, OnInit } from '@angular/core';
import { Observable }        from 'rxjs/Observable';
import 'rxjs/add/operator/finally';

import { Userdata }        from '../userdata';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  users: Observable<Userdata[]>;
  isLoading = false;
  selectedUser: Userdata;

  constructor(private userservice:UserService) { }

  ngOnInit() {
  	this.getUsers();
  	// console.log(this.users);
  }
  getUsers()
  {
  	this.isLoading=true;
  	this.users=this.userservice.getUsers();
  	this.selectedUser=undefined;
  }
  select(users:Userdata){this.selectedUser=users;}

}

