import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { BlogsComponent }      from './blogs/blogs.component';
import { BlogViewComponent } from './blog-view/blog-view.component';
import { AuthService } from './login/auth.service';
import { AuthGuard } from './auth.guard';
import { LoginComponent } from './login/login.component';
import { EditComponent } from './blogs/edit.component';
import { RegisterComponent } from './register/register.component';
import { AddBlogComponent } from './add-blog/add-blog.component';

const appRoutes: Routes = [
      {path: '', redirectTo: '/blogs', pathMatch: 'full' },
      { path: 'blogs', component: BlogsComponent,canActivate: [AuthGuard] },
      { path: 'addblogs', component: AddBlogComponent,canActivate: [AuthGuard] },
      { path: 'blogs/blogview/:id', component: BlogViewComponent },
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'blogs/edit/:id', component: EditComponent,canActivate: [AuthGuard] },
      { path: 'blogs/blogview/:ids/edit/:id', component: EditComponent,canActivate: [AuthGuard] },
      {
        path: 'users',
        loadChildren: 'app/users/users.module#UsersModule',
      }
  ];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  declarations: []
})
export class AppRoutingModule { 
	
}
