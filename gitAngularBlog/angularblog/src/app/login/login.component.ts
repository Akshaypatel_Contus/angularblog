import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';
import { NgModel } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	model: any = {};
    
    returnUrl: string;
  constructor(private route: ActivatedRoute,
        private router: Router,
        private authService: AuthService) { }

  ngOnInit() {
  		this.authService.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/blogs';
  }
  login() {
        
        this.authService.login(this.model.username, this.model.password)
            .subscribe(
                data => {
                    this.router.navigate([this.returnUrl]);
                });
    }

}
