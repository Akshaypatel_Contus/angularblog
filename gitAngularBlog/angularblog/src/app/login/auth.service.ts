import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';



@Injectable()
export class AuthService {
    
  constructor(private http: HttpClient) { }

    login(username, password) {
        return this.http.post('http://127.0.0.1:8000/api/login1',{ email: username, password: password }).map(user => {
                    localStorage.setItem('currentUser', username)
        });
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }

}
