import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './appheader.component.html',
  styleUrls: ['./appheader.component.css']
})
export class AppHeaderComponent implements OnInit {
	
	user=localStorage.getItem("currentUser");
  constructor() { }

  ngOnInit() {

  }
}
