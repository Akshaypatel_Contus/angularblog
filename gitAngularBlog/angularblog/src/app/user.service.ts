import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of }         from 'rxjs/observable/of';
import 'rxjs/add/operator/delay';

import { Userdata, users } from './userdata';


@Injectable()
export class UserService {
	delay=500;
  constructor() { }
  getUsers():Observable<Userdata[]>{
  	return of(users).delay(this.delay);
  }
  updateUsers(user:Userdata):Observable<Userdata>{
  	const oldUser=users.find(u=>u.id===user.id);
  	const newUser=Object.assign(oldUser,user);
  	return of(newUser).delay(this.delay);
  }
}
