import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {Routes, RouterModule, Router} from "@angular/router";
import { Observable } from 'rxjs/Observable';
import { NgModel } from '@angular/forms';
import 'rxjs/add/operator/map';


@Component({
  selector: 'app-add-blog',
  templateUrl: './add-blog.component.html',
  styleUrls: ['./add-blog.component.css']
})
export class AddBlogComponent implements OnInit {

  
	model: any = {};
	fileToUpload: File = null;
	handleFileInput(files: FileList) {

		console.log(files.item(0));
    	this.fileToUpload = files.item(0);
    	console.log(this.fileToUpload);
	}
  constructor(private http: HttpClient,private route: Router) { }

  ngOnInit() {
  }
  addblog()
  {
    const formData: FormData = new FormData();
    formData.append('blog_title',this.model.blog_title);
    formData.append('blog_desc',this.model.blog_desc);
    formData.append('blog_img',this.fileToUpload,this.fileToUpload.name);
    formData.append('user',localStorage.getItem('currentUser'));
  	console.log(formData);

    //console.log(this.model.blog_title);
    //console.log(this.model.blog_desc);
    //console.log(this.fileToUpload);


  	this.http.post('http://127.0.0.1:8000/api/blog_insert',formData).subscribe(user => {
        this.route.navigateByUrl('/blogs');
  	});
  }
}
