import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import * as _ from 'underscore';
import { PagerService } from '../service/index';

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css']
})
export class BlogsComponent implements OnInit {
	pager: any = {};

    // paged items
  pagedItems: any[];
	hero:any = [];
  constructor(private http: HttpClient,private pagerService: PagerService) { }
  user=localStorage.getItem("currentUser");
  ngOnInit() {
  this.http.get('http://127.0.0.1:8000/api/blogapi').subscribe(data => {
  	
      this.hero=data;
      this.setPage(1);
    });

  }
  setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        // get pager object from service
        this.pager = this.pagerService.getPager(this.hero.length, page);

        // get current page of items
        this.pagedItems = this.hero.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }
  	delete(id:string)
    {
      console.log(id);
      this.http.post('http://127.0.0.1:8000/api/deleteblog',{id:id}).subscribe(data => {
        this.hero=data;
        this.setPage(1);
      });
    }
    

}
