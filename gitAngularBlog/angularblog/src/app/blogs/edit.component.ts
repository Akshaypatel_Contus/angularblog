import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {Routes, RouterModule, Router,ActivatedRoute} from "@angular/router";
import { Observable } from 'rxjs/Observable';
import { NgModel } from '@angular/forms';
import 'rxjs/add/operator/map';


@Component({
  selector: 'app-edit-blog',
  templateUrl: './edit.component.html',
  styleUrls: ['./blogs.component.css']
})
export class EditComponent implements OnInit {
  
	model: any = {};
  image:any="";
	fileToUpload: File = null;
	handleFileInput(files: FileList) {

		console.log(files.item(0));
    	this.fileToUpload = files.item(0);
    	console.log(this.fileToUpload);
	}
  constructor(private http: HttpClient,private route: Router,private activatedRoute: ActivatedRoute) {
   }

  ngOnInit() {
    
    let id=this.activatedRoute.snapshot.paramMap.get('id');
    console.log(id); // Print the parameter to the console. 
    this.http.post('http://127.0.0.1:8000/api/editblog',{id:id}).subscribe(data => {
      console.log(data);
      this.model.blog_title=data[0].blog_title;
      this.model.blog_desc=data[0].blog_desc;
      this.image="http://127.0.0.1:8000/"+data[0].blog_img;
    });
  }
  updateblog()
  {
    let id=this.activatedRoute.snapshot.paramMap.get('id');
    const formData: FormData = new FormData();
    formData.append('blog_title',this.model.blog_title);
    formData.append('blog_desc',this.model.blog_desc);
    if(this.fileToUpload!=null)
    {
      formData.append('blog_img',this.fileToUpload,this.fileToUpload.name);
    }
    formData.append('id',id);
    formData.append('user',localStorage.getItem('currentUser'));
  	console.log(formData);

    //console.log(this.model.blog_title);
    //console.log(this.model.blog_desc);
    //console.log(this.fileToUpload);


  	this.http.post('http://127.0.0.1:8000/api/blog_update',formData).subscribe(user => {
    console.log(user);
        this.route.navigateByUrl('/blogs');
  	});
  }
}
